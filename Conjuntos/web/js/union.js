class Conjunto {
    static declaracion() {
        let domesticos4patas = ["Vaca", "Caballo", "Cerdo"];
        let domesticosPlumas = ["Gallina","Pato","Ganso"];
        console.log(domesticos4patas);
        console.log(domesticosPlumas);
        
        let domesticos4patasMostrar = domesticos4patas.map( animal => 
        `
                    <li>${animal}</li>
                `
                ).join('');
        document.querySelector("#domesticos4patasPnl").innerHTML = domesticos4patasMostrar;
        
        
        let domesticosPlumasMostrar = domesticosPlumas.map( animal => 
        `
                    <li>${animal}</li>
                `
                ).join('');
        document.querySelector("#domesticosPlumasPnl").innerHTML = domesticosPlumasMostrar;
        
        this.union(domesticos4patas, domesticosPlumas);
    }
    
    static union(a, b) {
        let unionresult = [];        
        a.map( elemento => unionresult.push(elemento));
        b.map( elemento => unionresult.push(elemento));
        console.log(unionresult);        
        //Para elementos unicos
        console.log(new Set(unionresult));                
        let conjuntosUnidos = unionresult.map( elemento =>  
                `
                <li>${elemento}</li>
                `).join('');        
        document.querySelector("#unionResultadoPnl").innerHTML = conjuntosUnidos;                        
    }
    
    
    static main() {
        this.declaracion();
    }
}

Conjunto.main();

